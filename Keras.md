# Kerasの使い方

## install
```angular2html
pip install keras
```

## 機能
### Dense
通常の全結合ニューラルネットワークレイヤー．
- units：正の整数，出力空間の次元数
- activation： 使用する活性化関数名 もしあなたが何も指定しなければ，活性化は適用されない． （すなわち，"線形"活性化： a(x) = x）．
- use_bias： レイヤーがバイアスベクトルを使用するかどうか．
- kernel_initializer： kernel重み行列の初期化
- bias_initializer： バイアスベクトルの初期化
- kernel_regularizer： kernel重み行列に適用される正則化関数
- bias_regularizer： バイアスベクトルに適用される正則化関数
- activity_regularizer： レイヤーの出力に適用される正則化関数（"activation"）
- kernel_constraint： kernel重み行列に適用される制約関数
- bias_constraint： バイアスベクトルに適用される制約関数

### Conv2D
2次元入力をフィルターする畳み込み層．
- filters: 使用するカーネルの数．
- kernel_size: 畳み込みカーネルの幅と高さを指定します. タプル/リストでカーネルの幅と高さをそれぞれ指定でき，整数の場合は正方形のカーネルになります．
- strides: カーネルのストライドを指定します. 2つの整数からなるタプル/リストで縦と横のストライドをそれぞれ指定でき，整数の場合は幅と高さが同様のストライドになります．
- padding: "valid"か"same"のどちらかを指定します．
- data_format: "channels_last"（デフォルト）か"channels_first"を指定します. "channels_last"の場合，入力のshapeは"(batch, height, width, channels)"となり，"channels_first"の場合は"(batch, channels, height, width)"となります．デフォルトはKerasの設定ファイル~/.keras/keras.jsonのimage_data_formatの値です．一度も値を変更していなければ，"channels_last"になります．
- dilation_rate: 膨張率．整数か2つの整数からなるタプル/リストを指定します．単一の整数の場合，それぞれの次元に同一の値が適用されます．現在，dilation_rate value != 1 とすると，strides value != 1を指定することはできません．
- activation: 使用する活性化関数の名前（activationsを参照）， 何も指定しなければ，活性化は一切適用されません（つまり"線形"活性a(x) = x）．
- use_bias: 真理値で，バイアスベクトルを加えるかどうかを指定します．
- kernel_initializer: カーネルの重み行列の初期値を指定します．
- bias_initializer: バイアスベクトルの初期値を指定します．
- kernel_regularizer: カーネルの重み行列に適用させるRegularizerを指定します．
- bias_regularizer: バイアスベクトルに適用させるRegularizerを指定します．
- activity_regularizer: 出力テンソルに適用させるRegularizerを指定します．
- kernel_constraint: カーネルの行列に適用させるConstraintを指定します．
- bias_constraint: バイアスベクトルに適用させるConstraintを指定します．

### MaxPooling2D
空間データのマックスプーリング演算．
- pool_size: ダウンスケールする係数を決める 2つの整数のタプル（垂直，水平）． (2, 2) は画像をそれぞれの次元で半分にします．
- strides: ストライド値．2つの整数からなるタプル，もしくはNoneで指定します． Noneの場合は，pool_sizeの値が適用されます．
- padding: 'valid'か'same'のいずれかです．
- data_format: "channels_last"（デフォルト）か"channels_first"を指定します. "channels_last"の場合，入力のshapeは(batch, height, width, channels)となり，"channels_first"の場合は(batch, channels, height, width)となります．デフォルトはKerasの設定ファイル~/.keras/keras.jsonのimage_data_formatの値です．一度も値を変更していなければ，"channels_last"になります．

### Dropout
入力にドロップアウトを適用する．ドロップアウトは，訓練時のそれぞれの更新において入力ユニットのrateをランダムに0にセットすることであり，それは過学習を防ぐのを助ける．
- rate： 0と1の間の浮動小数点数．入力ユニットをドロップする割合．
- noise_shape： 入力と乗算されるバイナリドロップアウトマスクのshapeは1階の整数テンソルで表す．例えば入力のshapeを(batch_size, timesteps, features)とし，ドロップアウトマスクをすべてのタイムステップで同じにしたい場合，noise_shape=(batch_size, 1, features)を使うことができる.
- seed： random seedとして使うPythonの整数．


### Flatten
入力を平滑化する．バッチサイズに影響されない．



## Mnist
### Sequential
層を積み重ねて行くイメージ

```python
from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])
```
addでどんどん層を追加していく

### Functional
層を繋げていくイメージ

```python
from keras import backend as K
from keras.utils import plot_model

batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

inputs = Input(input_shape)
x = Conv2D(32, kernel_size=(3, 3), activation='relu')(inputs)
x = Conv2D(64, (3, 3), activation='relu')(x)
x = MaxPooling2D(pool_size=(2, 2))(x)
x = Dropout(0.25)(x)
x = Flatten()(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.5)(x)
outputs = Dense(output_dim=num_classes, activation='softmax')(x)

model = Model(inputs=inputs, outputs=outputs)

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])
```

Inputが新たに必要

## モデルの可視化
### 追加のインストール
```commandline
brew install gts
brew install graphviz
pip install pydot
pip install graphviz
```

### プログラムの追記
```python
from keras.utils import plot_model
plot_model(model, to_file="model.png")
```

### 可視化結果
![モデル](./model.png)

